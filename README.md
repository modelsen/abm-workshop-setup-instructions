# Setup instructions for the DH 2023 Workshop "Growing and pruning the Republic of Letters. Learning to simulate the past with agent-based modeling using the ‘mesa’ package"

Welcome dear workshop participant! 

This README will guide you through the setup of a conda environment and a jupyter kernel which you will need for the workshop's excercises and gives an overview of the workshops goals. 

If you are already familiar with setting up a conda environment and a kernel for jupyter notebooks, you can just import the environment with the .yml file attached to the setup kit we sent you, create a new kernel and activate it here. Please do test the imports via the "ABM workshop setup" jupyter notebook to make sure everything worked as intended.

## About the workshop

The goal of the workshop is twofold. On the one hand, it is to give you practical introduction to the method of agent-based modeling. For this purpose, we will try out and discuss possible approaches to modeling using the ‘LetterSpace’ model, which we have developed for the ‘modelSen’ project. On the other hand, already during the workshop we want to discuss with you and reflect upon potentials and limitations of the method as well as possible ways forward for you in applying this method in your own research. In the end, we hope that you will have a solid basis both practically and theoretically to continue learning about agent-based modeling and simulations on your own.

## Technical setup

### Install Jupyter

This workshop will use jupyter notebooks, an interactive web application for creating and sharing computational documents. If you already have jupyter and know how to use it you can open the `abm-workshop-setup.ipynb` and proceed with the next step. 

To open and create jupyter notebooks, you will need to install jupyter! You can do this simply by typing `pip install jupyter` and `pip install notebook` in the command line. However, we would suggest to download and install [Anaconda](https://www.anaconda.com/products/distribution), which features jupyter as well as conda, which you will also need to create a new environment in the next step.

If you open jupyter via command line (`jupyter notebook`) or via the Anaconda Navigation, a new browser tab opens where you can navigate through your local file structure and open/create jupyter notebooks. You can now browse for the `abm-workshop-setup.ipynb` file (that format denotes jupyter notebooks) and open it.

### Create and activate conda environment

To create a new environment for this workshop, you will need a conda distribution. If you don't already have it, the easiest and most complete way to get this is to download and install [Anaconda](https://www.anaconda.com/products/distribution). Follow the instructions there. 

Next, we recommend that you open a new terminal/command line to create the environment. To build a new environment from the settings we provide in the `environment.yml` file, type in or copy `conda env create -f abm-environment.yml -n abm-workshop-env`. 

Once it is installed successfully, activate it by typing or copying `conda activate abm-workshop-env`.

### Install a new kernel for jupyter

Next, you will need to install your environment as kernel for jupyter, so it can be activated in the jupyter notebook as well.

In the command line/terminal, with the environment activated, type in or copy `python -m ipykernel install --user --name=abm-workshop-env --display-name="abm-workshop-env"`. This creates a new kernel for jupyter under the specified name. Existing environments of the same name will be overwritten.

If you already opened the `abm-workshop-setup.ipynb`, you need to refresh the page for jupyter to notice the new kernel! Otherwise, search the file and open it now.

*******

Once refreshed/opened, click on `Kernel` in the top bar of the page. Hover over the option `Change kernel`. There your new environment/kernel should appear under the name you specified, `abm-workshop-env` in this example. Click to activate the new kernel!

Now you should be set up to use our notebooks and follow the exercises.

## Help

If any errors occur, double check if you followed all the instructions closely. Sometimes, there can be a problem with the installation of the packages. In that case, it is best to build a new environment from scratch. 

In a new command line/terminal, type `conda create -n abm-workshop-env python` to create a fresh environment and activate it with `conda activate abm-workshop-env`.

Then proceed step-by-step with the follwing installations:

`conda install numpy`
    
`conda install pandas`

`conda install -c conda-forge mesa`

`conda install -c conda-forge cartopy`

`conda install ipykernel`

`pip install scicom`

`python -m ipykernel install --user --name=abm-workshop --display-name="abm-workshop"`

Now you can refresh the jupyter page, activate the new kernel and try the imports again. 

If this also doesn't work, or if you have any other questions, don't hesitate to contact Jascha Schmitz (jmschmitz@mpiwg-berlin.mpg.de) to help you!