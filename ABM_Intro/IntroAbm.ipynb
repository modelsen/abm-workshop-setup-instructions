{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Mesa\n",
    "\n",
    "This notebook is based on the official tutorials of [Mesa](https://mesa.readthedocs.io). We have adapted it to suit the needs of our workshop and flavored it to be more about history.\n",
    "\n",
    "Mesa is a Python framework for agent-based modeling. Getting started with Mesa is easy. In this tutorial, we will walk through creating a simple model and progressively add functionality which will illustrate Mesa’s core features.\n",
    "\n",
    "The base for this tutorial is a very simple model of agents exchanging letters. Next, we add space to allow agents to move. Then, we’ll cover two of Mesa’s analytic tools: the data collector and batch runner. After that, we’ll add an interactive visualization which lets us watch the model as it runs. \n",
    "\n",
    "You can find examples of other simple models in the [examples directory](https://github.com/projectmesa/mesa-examples) of the Mesa repository.\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Model description\n",
    "\n",
    "The tutorial model is a very simple simulated agent-based letter network. The rules of our tutorial model:\n",
    "\n",
    "1. There are some number of agents.\n",
    "\n",
    "2. All agents begin with 0 letters.\n",
    "\n",
    "3. At every step of the model, an agent sends a letter to some other agent, based either on randomness or distance to the other agents. \n",
    "\n",
    "Let’s get started!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"To start with, let's import the mesa module\"\"\"\n",
    "import mesa"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting up the model\n",
    "To begin writing the model code, we start with two core classes: one for the overall `model`, the other for the `agents`. The model class holds the model-level attributes, manages the agents, and generally handles the global level of our model. Each instantiation of the model class will be a specific model run. Each model will contain multiple agents, all of which are instantiations of the agent class. Both the model and agent classes are child classes of Mesa’s generic [Model and Agent classes](https://mesa.readthedocs.io/en/stable/apis/init.html).\n",
    "\n",
    "This is seen in the code with `class LetterModel(mesa.Model)` or `class LetterAgent(mesa.Agent)`.\n",
    "\n",
    "For now, each agent has only two variables: how much letters it currently has sent and received. Each agent will also have a unique identifier (i.e., a name), stored in the `unique_id` variable. Giving each agent a unique id is a good practice when doing agent-based modeling.\n",
    "\n",
    "There is only one model-level parameter: how many agents the model contains. When a new model is started, we want it to populate itself with the given number of agents.\n",
    "\n",
    "The beginning of both classes looks like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class LetterAgent(mesa.Agent):\n",
    "    \"\"\"An agent with a fixed amount of initial letters, namely 0.\"\"\"\n",
    "\n",
    "    def __init__(self, unique_id, model):\n",
    "        super().__init__(unique_id, model)\n",
    "        self.letters_sent = 0\n",
    "        self.letters_received = 0\n",
    "\n",
    "\n",
    "class LetterModel(mesa.Model):\n",
    "    \"\"\"A model with some number of agents.\"\"\"\n",
    "\n",
    "    def __init__(self, N):\n",
    "        self.num_agents = N\n",
    "        # Create N number of agents\n",
    "        for i in range(self.num_agents):\n",
    "            a = LetterAgent(i, self)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Adding the scheduler \n",
    "\n",
    "Time in most agent-based models moves in steps, sometimes also called ticks. At each step of the model, one or more of the agents – usually all of them – are activated and take their own step, changing internally and/or interacting with one another or the environment.\n",
    "\n",
    "The scheduler is a special model component which controls the order in which agents are activated. For example, all the agents may activate in the same order every step, their order might be shuffled, we may try to simulate all the agents acting at the same time, and more. Mesa offers a few different built-in scheduler classes, with a common interface. That makes it easy to change the activation regime a given model uses, and see whether it changes the model behavior. This may not seem important, but scheduling patterns can have an impact on your results [Comer2014].\n",
    "\n",
    "For now, let's use one of the simplest ones: `RandomActivation`*, which activates all the agents once per step, in random order. Every agent is expected to have a ``step`` method. The step method is the action the agent takes when it is activated by the model schedule. We add an agent to the schedule using the `add` method; when we call the schedule's `step` method, the model shuffles the order of the agents, then activates and executes each agent's ```step``` method.\n",
    "\n",
    "*Unlike `mesa.model` or `mesa.agent`, `mesa.time` has multiple classes (e.g. `RandomActivation`, `StagedActivation` etc). To ensure context, time is used in the import as evidenced below with `mesa.time.Randomactivation`.  You can see the different time classes as [mesa.time](https://github.com/projectmesa/mesa/blob/main/mesa/time.py). \n",
    "\n",
    "With that in mind, the model code with the scheduler added looks like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class LetterAgent(mesa.Agent):\n",
    "    \"\"\"An agent with a fixed amount of initial letters, namely 0.\"\"\"\n",
    "\n",
    "    def __init__(self, unique_id, model):\n",
    "        super().__init__(unique_id, model)\n",
    "        self.letters_sent = 0\n",
    "        self.letters_received = 0\n",
    "\n",
    "    def step(self):\n",
    "        # The agent's step will go here.\n",
    "        # For demonstration purposes we will print the agent's unique_id\n",
    "        print(\"Hi, I am agent \" + str(self.unique_id) + \".\")\n",
    "\n",
    "class LetterModel(mesa.Model):\n",
    "    \"\"\"A model with some number of agents.\"\"\"\n",
    "\n",
    "    def __init__(self, N):\n",
    "        self.num_agents = N\n",
    "        self.schedule = mesa.time.RandomActivation(self)\n",
    "        # Create N number of agents\n",
    "        for i in range(self.num_agents):\n",
    "            a = LetterAgent(i, self)\n",
    "            self.schedule.add(a)\n",
    "            \n",
    "    def step(self):\n",
    "        \"\"\"Advance the model by one step.\"\"\"\n",
    "        self.schedule.step()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At this point, we have a model which runs – it just doesn’t do anything. You can see for yourself with a few easy lines:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "empty_model = LetterModel(10) # create a model with 10 agents\n",
    "empty_model.step() # execute the step function once"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Agent Step\n",
    "\n",
    "Now we just need to have the agents do what we intend for them to do: send each other letters. \n",
    "\n",
    "To allow the agent to choose another agent at random, we use the `model.random` random-number generator. This works just like Python’s `random` module, but with a fixed seed set when the model is instantiated, that can be used to replicate a specific model run later.\n",
    "\n",
    "To pick an agent at random, we need a list of all agents. Notice that there isn’t such a list explicitly in the model. The scheduler, however, does have an internal list of all the agents it is scheduled to activate.\n",
    "\n",
    "With that in mind, we rewrite the agent `step` method, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class LetterAgent(mesa.Agent):\n",
    "    \"\"\"An agent with a fixed amount of initial letters, namely 0.\"\"\"\n",
    "\n",
    "    def __init__(self, unique_id, model):\n",
    "        super().__init__(unique_id, model)\n",
    "        self.letters_sent = 0\n",
    "        self.letters_received = 0\n",
    "\n",
    "    def step(self):\n",
    "\n",
    "        other_agent = self.random.choice(self.model.schedule.agents)\n",
    "        other_agent.letters_received += 1\n",
    "        self.letters_sent += 1"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running your first model\n",
    "\n",
    "With that last piece in hand, it’s time for the first rudimentary run of the model. Let’s create a model with 10 agents, and run it for 10 steps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = LetterModel(10)\n",
    "for i in range(20):\n",
    "    model.step()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we need to get some data out of the model. \n",
    "Specifically, we want to see how many letters each agent sent and received. We can get the letters values with list comprehension, and then use `matplotlib` (or another graphics library) to visualize the data in a histogram."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# import matplotlib for our visualization\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "plt.show()\n",
    "\n",
    "#agent_letters_sent = [a.letters_sent for a in model.schedule.agents]\n",
    "agent_letters_recd = [b.letters_received for b in model.schedule.agents]\n",
    "\n",
    "#plt.hist(agent_letters_sent)\n",
    "plt.hist(agent_letters_recd)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You’ll should see something like the distribution above. Yours will almost certainly look at least slightly different, since each run of the model is random, after all.\n",
    "\n",
    "To get a better idea of how a model behaves, we can create multiple model runs and see the distribution that emerges from all of them. We can do this with a nested for loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "all_letters = []\n",
    "\n",
    "# This runs the model 100 times, each model executing 10 steps.\n",
    "for j in range(100):\n",
    "    # Run the model\n",
    "    model = LetterModel(10)\n",
    "    for i in range(10):\n",
    "        model.step()\n",
    "\n",
    "    # Store the results\n",
    "    for agent in model.schedule.agents:\n",
    "        all_letters.append(agent.letters_received)\n",
    "\n",
    "plt.hist(all_letters, bins=range(max(all_letters) + 1))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This runs 100 instantiations of the model, and runs each for 10 steps. (Notice that we set the histogram bins to be integers, since agents can only have whole numbers of letters). This distribution looks a lot smoother. By running the model 100 times, we smooth out some of the ‘noise’ of randomness, and get to the model’s overall expected behavior.\n",
    "\n",
    "For now, the letter distribution looks pretty normal, because it is basically random. These results are not too surprising, yet. Let's add some more comparably realistic behavior by introducing space between the agents and let that influence the letter sending decision."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Adding space\n",
    "\n",
    "Many ABMs have a spatial element, with agents moving around and interacting with nearby neighbors. Mesa currently supports two overall kinds of spaces:\n",
    "`grid`, and `continuous`. Grids are divided into cells, and agents can only be on a particular cell, like pieces on a chess board. Continuous space, in contrast, allows agents to have any arbitrary position. Both grids and continuous spaces are frequently toroidal, meaning that the edges wrap around, with cells on the right edge connected to those on the left edge, and the top to the bottom. This prevents some cells having fewer neighbors than others, or agents being able to go off the edge of the environment.\n",
    "\n",
    "Let’s add a simple spatial element to our model by putting our agents on a grid and make them walk around at random. Instead of sending a letter to any random agent, they’ll give it to an agent on the same cell.\n",
    "\n",
    "Mesa has two main types of grids: `SingleGrid` and `MultiGrid`*. `SingleGrid` enforces at most one agent per cell; `MultiGrid` allows multiple agents to be in the same cell. Since we want agents to be able to share a cell, we use `MultiGrid`.\n",
    "\n",
    "*However there are more types of space to include `HexGrid`, `NetworkGrid`, and the previously mentioned `ContinuousSpace`. Similar to `mesa.time` context is retained with `mesa.space.[enter class]`. You can see the different classes as `mesa.space`.\n",
    "\n",
    "We instantiate a grid with width and height parameters, and a boolean as to whether the grid is toroidal. Let’s make width and height model parameters, in addition to the number of agents, and have the grid always be toroidal. We can place agents on a grid with the grid’s `place_agent` method, which takes an agent and an (x, y) tuple of the coordinates to place the agent."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class LetterModel(mesa.Model):\n",
    "    \"\"\"A model with some number of agents.\"\"\"\n",
    "\n",
    "    def __init__(self, N, width, height):\n",
    "        self.num_agents = N\n",
    "        self.grid = mesa.space.MultiGrid(width, height, True)\n",
    "        self.schedule = mesa.time.RandomActivation(self)\n",
    "\n",
    "        # Create agents\n",
    "        for i in range(self.num_agents):\n",
    "            a = LetterAgent(i, self)\n",
    "            self.schedule.add(a)\n",
    "\n",
    "            # Add the agent to a random grid cell\n",
    "            x = self.random.randrange(self.grid.width)\n",
    "            y = self.random.randrange(self.grid.height)\n",
    "            self.grid.place_agent(a, (x, y))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Under the hood, each agent’s position is stored in two ways: the agent is contained in the grid in the cell it is currently in, and the agent has a `pos` variable with an (x, y) coordinate tuple. The `place_agent` method adds the coordinate to the agent automatically.\n",
    "\n",
    "Now we need to add to the agents’ behaviors, letting them move around and only send letters to other agents in the same cell.\n",
    "\n",
    "First let’s handle movement, and have the agents move to a neighboring cell. The grid object provides a `move_agent` method, which like you’d imagine, moves an agent to a given cell. That still leaves us to get the possible neighboring cells to move to. There are a couple ways to do this. One is to use the current coordinates, and loop over all coordinates +/- 1 away from it. For example:\n",
    "\n",
    "```python\n",
    "neighbors = []\n",
    "x, y = self.pos\n",
    "for dx in [-1, 0, 1]:\n",
    "    for dy in [-1, 0, 1]:\n",
    "        neighbors.append((x+dx, y+dy))\n",
    "```"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But there’s an even simpler way, using the grid’s built-in `get_neighborhood` method, which returns all the neighbors of a given cell. This method can get two types of cell neighborhoods: [Moore](https://en.wikipedia.org/wiki/Moore_neighborhood) (includes all 8 surrounding squares), and [Von Neumann](https://en.wikipedia.org/wiki/Von_Neumann_neighborhood) (only including the 4 top, bottom, left and right neighboring squares). It also needs an argument as to whether to include the center cell itself as one of the neighbors.\n",
    "\n",
    "With that in mind, the agent’s move method looks like this:\n",
    "\n",
    "```python\n",
    "class LetterAgent(mesa.Agent):\n",
    "   #...\n",
    "    def move(self):\n",
    "        possible_steps = self.model.grid.get_neighborhood(\n",
    "            self.pos,\n",
    "            moore=True,\n",
    "            include_center=False)\n",
    "        new_position = self.random.choice(possible_steps)\n",
    "        self.model.grid.move_agent(self, new_position)\n",
    "```\n",
    "\n",
    "Next, we need to get all the other agents present in a cell, and sent them a letter. We can get the contents of one or more cells using the grid's `get_cell_list_contents` method, or by accessing a cell directly. The method accepts a list of cell coordinate tuples, or a single tuple if we only care about one cell.\n",
    "\n",
    "```python\n",
    "class LetterAgent(mesa.Agent):\n",
    "    #...\n",
    "    def send_letter(self):\n",
    "        cellmates = self.model.grid.get_cell_list_contents([self.pos])\n",
    "        if len(cellmates) > 1:\n",
    "            other = self.random.choice(cellmates)\n",
    "            other.letters_received += 1\n",
    "            self.letters_sent -= 1\n",
    "```\n",
    "\n",
    "And with those two methods, the agent's ``step`` method becomes:\n",
    "\n",
    "```python\n",
    "class LetterAgent(mesa.Agent):\n",
    "    # ...\n",
    "    def step(self):\n",
    "        self.move()\n",
    "        self.send_letter()\n",
    "```\n",
    "\n",
    "Now, putting that all together should look like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class LetterAgent(mesa.Agent):\n",
    "    \"\"\"An agent with letters sent and received.\"\"\"\n",
    "\n",
    "    def __init__(self, unique_id, model):\n",
    "        super().__init__(unique_id, model)\n",
    "        self.letters_sent = 0\n",
    "        self.letters_received = 0\n",
    "\n",
    "    def move(self):\n",
    "        possible_steps = self.model.grid.get_neighborhood(\n",
    "            self.pos, moore=True, include_center=False\n",
    "        )\n",
    "        new_position = self.random.choice(possible_steps)\n",
    "        self.model.grid.move_agent(self, new_position)\n",
    "\n",
    "    def send_letter(self):\n",
    "        cellmates = self.model.grid.get_cell_list_contents([self.pos])\n",
    "        if len(cellmates) > 1:\n",
    "            other_agent = self.random.choice(cellmates)\n",
    "            other_agent.letters_received += 1\n",
    "            self.letters_sent += 1\n",
    "\n",
    "    def step(self):\n",
    "        self.move()\n",
    "        self.send_letter()\n",
    "\n",
    "\n",
    "class LetterModel(mesa.Model):\n",
    "    \"\"\"A model with some number of agents.\"\"\"\n",
    "\n",
    "    def __init__(self, N, width, height):\n",
    "        self.num_agents = N\n",
    "        self.grid = mesa.space.MultiGrid(width, height, True)\n",
    "        self.schedule = mesa.time.RandomActivation(self)\n",
    "        # Create agents\n",
    "        for i in range(self.num_agents):\n",
    "            a = LetterAgent(i, self)\n",
    "            self.schedule.add(a)\n",
    "            # Add the agent to a random grid cell\n",
    "            x = self.random.randrange(self.grid.width)\n",
    "            y = self.random.randrange(self.grid.height)\n",
    "            self.grid.place_agent(a, (x, y))\n",
    "\n",
    "    def step(self):\n",
    "        self.schedule.step()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's create a model with 50 agents on a 10x10 grid, and run it for 20 steps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = LetterModel(50, 10, 10)\n",
    "for i in range(20):\n",
    "    model.step()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's use `matplotlib` and `numpy` to visualize the number of agents residing in each cell. To do that, we create a numpy array of the same size as the grid, filled with zeros. Then we use the grid object's `coord_iter()` feature, which lets us loop over every cell in the grid, giving us each cell's coordinates and contents in turn."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "agent_counts = np.zeros((model.grid.width, model.grid.height))\n",
    "for cell in model.grid.coord_iter():\n",
    "    cell_content, x, y = cell\n",
    "    agent_count = len(cell_content)\n",
    "    agent_counts[x][y] = agent_count\n",
    "plt.imshow(agent_counts, interpolation=\"nearest\")\n",
    "plt.colorbar()\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Collecting Data\n",
    "\n",
    "So far, at the end of every model run, we've had to go and write our own code to get the data out of the model. This has two problems: it isn't very efficient, and it only gives us end results. If we wanted to know the letter counts of each agent at each step, we'd have to add that to the loop of executing steps, and figure out some way to store the data.\n",
    "\n",
    "Since one of the main goals of agent-based modeling is generating data for analysis, Mesa provides a class which can handle data collection and storage for us and make it easier to analyze.\n",
    "\n",
    "The data collector stores three categories of data: model-level variables, agent-level variables, and tables (which are a catch-all for everything else). Model- and agent-level variables are added to the data collector along with a function for collecting them. Model-level collection functions take a model object as an input, while agent-level collection functions take an agent object as an input. Both then return a value computed from the model or each agent at their current state. When the data collector’s `collect` method is called, with a model object as its argument, it applies each model-level collection function to the model, and stores the results in a dictionary, associating the current value with the current step of the model. Similarly, the method applies each agent-level collection function to each agent currently in the schedule, associating the resulting value with the step of the model, and the agent’s `unique_id`.\n",
    "\n",
    "Let's add a `DataCollector` to the model with [`mesa.DataCollector`](https://github.com/projectmesa/mesa/blob/main/mesa/datacollection.py), and collect two variables at the agent level. We want to collect every agent's letters sent and letters received at every step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class LetterAgent(mesa.Agent):\n",
    "    \"\"\"An agent with letters sent and letters received.\"\"\"\n",
    "\n",
    "    def __init__(self, unique_id, model):\n",
    "        super().__init__(unique_id, model)\n",
    "        self.letters_sent = 0\n",
    "        self.letters_received = 0\n",
    "\n",
    "    def move(self):\n",
    "        possible_steps = self.model.grid.get_neighborhood(\n",
    "            self.pos, moore=True, include_center=False\n",
    "        )\n",
    "        new_position = self.random.choice(possible_steps)\n",
    "        self.model.grid.move_agent(self, new_position)\n",
    "\n",
    "    def send_letter(self):\n",
    "        cellmates = self.model.grid.get_cell_list_contents([self.pos])\n",
    "        if len(cellmates) > 1:\n",
    "            other_agent = self.random.choice(cellmates)\n",
    "            other_agent.letters_received += 1\n",
    "            self.letters_sent += 1\n",
    "\n",
    "    def step(self):\n",
    "        self.move()\n",
    "        self.send_letter()\n",
    "\n",
    "\n",
    "class LetterModel(mesa.Model):\n",
    "    \"\"\"A model with some number of agents.\"\"\"\n",
    "\n",
    "    def __init__(self, N, width, height):\n",
    "        self.num_agents = N\n",
    "        self.grid = mesa.space.MultiGrid(width, height, True)\n",
    "        self.schedule = mesa.time.RandomActivation(self)\n",
    "        # Create agents\n",
    "        for i in range(self.num_agents):\n",
    "            a = LetterAgent(i, self)\n",
    "            self.schedule.add(a)\n",
    "            # Add the agent to a random grid cell\n",
    "            x = self.random.randrange(self.grid.width)\n",
    "            y = self.random.randrange(self.grid.height)\n",
    "            self.grid.place_agent(a, (x, y))\n",
    "\n",
    "        self.datacollector = mesa.DataCollector(\n",
    "            agent_reporters={\"Letters_sent\": \"letters_sent\", \"Letters_received\": \"letters_received\"}\n",
    "        )\n",
    "\n",
    "    def step(self):\n",
    "        self.datacollector.collect(self)\n",
    "        self.schedule.step()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At every step of the model, the datacollector will collect and store each agent's letters sent value.\n",
    "\n",
    "We run the model just as we did above. The `DataCollector` can export the data its collected as a pandas `DataFrame`, for easy interactive analysis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = LetterModel(50, 10, 10)\n",
    "for i in range(100):\n",
    "    model.step()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now get the agent-letters data like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "agent_letters = model.datacollector.get_agent_vars_dataframe()\n",
    "agent_letters.tail()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You'll see that the DataFrame's index is pairings of model step and agent ID. You can analyze it the way you would any other DataFrame. For example, to get a histogram of agent's letters sent at the model's end:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "end_letters = agent_letters.xs(99, level=\"Step\")[\"Letters_sent\"]\n",
    "end_letters.hist(bins=range(agent_letters.Letters_sent.max() + 1))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or to plot the letters received of a given agent (in this example, agent 14):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "one_agent_letters = agent_letters.xs(14, level=\"AgentID\")\n",
    "one_agent_letters.Letters_received.plot()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also use `pandas` to export the data to a CSV (comma separated value), which can be opened by any common spreadsheet application or opened by `pandas`.\n",
    "\n",
    "If you do not specify a file path, the file will be saved in the local directory. After you run the code below you will see two files appear (*model_data.csv* and *agent_data.csv*)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# save the agent data (stored in the pandas agent_letters object) to CSV\n",
    "agent_letters.to_csv(\"agent_data.csv\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Next steps and open questions\n",
    "\n",
    "Now we have a very simple model where agents send each other letters when they are close enough to each other, but otherwise walking aimlessly through their grid-like world. Of course, this model does not yet capture actual letter sending in a meaningful way. \n",
    "\n",
    "In the real world, for example in the time of the Republic of Letters, people wouldn't have wandered at random, but maybe stayed at certain places and only on special, purposeful occasions change place. They would also know and remember the people they write to or receive letters from. Based on their familiarity and, crucially, the topical content of the communication, people would choose to write certain people more than others, not just their immediate neighbors. Out of this, an actually interesting social-epistemic network might arise and be analysed. \n",
    "\n",
    "All of these features would be possible to implement with python and the simple methods we mentioned here and most of these we actually tried to model in the projects actual LetterModel on which we will have a look at next!\n",
    "\n",
    "If you want more information and a more thorough introduction to mesa, you can either head to [the documentation of mesa](https://mesa.readthedocs.io/en/stable/overview.html), which features a variant of this tutorial and more on advanced features, especially built-in javascript-based visualization methods, or you can head to youtube [for a video tutorial](https://www.youtube.com/playlist?list=PLF0b3ThojznRpQOd7iFukqXybbMV_vwZn) in a similar vein like the official mesa tutorials. "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Literature\n",
    "\n",
    "[Comer2014] Comer, Kenneth W. “Who Goes First? An Examination of the Impact of Activation on Outcome Behavior in AgentBased Models.” George Mason University, 2014. http://mars.gmu.edu/bitstream/handle/1920/9070/Comer_gmu_0883E_10539.pdf"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "abm-workshop",
   "language": "python",
   "name": "abm-workshop"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
